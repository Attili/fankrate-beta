from django.db import models
from django.contrib.postgres.fields import JSONField


class ProductCategory(models.Model):
    half_sleeve_men = 1
    full_sleeve_men = 2
    hoodies_men = 3
    half_sleeve_women = 4
    crop_top_women = 5
    phone_case = 6
    phone_grip = 7
    mug = 8

    categories = (
        (half_sleeve_men, 'Half sleeve t-shirt'),
        (full_sleeve_men, 'Full sleeve t-shirt'),
        (hoodies_men, 'Hoodies'),
        (half_sleeve_women, 'Half sleeve t-shirt women'),
        (crop_top_women, 'Crop top'),
        (phone_case, 'Phone case'),
        (phone_grip, 'Phone grip'),
        (mug, 'Mug'))

    category = models.IntegerField(
        choices=categories,
        null=True)
    is_men = models.BooleanField(
        default=True)
    is_women = models.BooleanField(
        default=True)

    class Meta:
        db_table = 'product_categories'


class Products(models.Model):
    product_category = models.ForeignKey(
        ProductCategory,
        on_delete=models.CASCADE)
    name = models.CharField(
        max_length=50)
    description = models.CharField(
        help_text='Product description',
        max_length=2000)
    images = JSONField(
        help_text='JSON of images link of the product')
    csq = JSONField(
        help_text='Color, size and quantities Json')
    order_count = models.IntegerField(
        blank=False,
        default=0,)
    price = models.DecimalField(
        max_digits=15,
        decimal_places=2)
    selling_price = models.DecimalField(
        max_digits=15,
        decimal_places=2)
    actual_price = models.DecimalField(
        max_digits=15,
        help_text='price offered by dropship/ actual cost',
        decimal_places=2)
    profit = models.DecimalField(
        max_digits=15,
        help_text='selling price - actual price - shipping cost',
        decimal_places=2)
    is_deleted = models.BooleanField(
        default=False
    )
    in_stock = models.BooleanField(
        default=True)
    is_cod_available = models.BooleanField(
        default=False)
    is_dropship_product = models.BooleanField(
        default=True)
    design_sku = models.CharField(
        max_length=255,
        blank=False)
    phone_model = models.CharField(
        max_length=255,
        blank=True,
        help_text='Phone model name',
        null=True)

    class Meta:
        db_table = 'products'
