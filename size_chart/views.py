from django.shortcuts import render, redirect
from django.views.generic import View
from django.http import HttpResponseRedirect


class SizeChart(View):
    template_name = 'size_chart/index.html'
    authentication_classes = ()

    def get(self, request):

        return render(request, self.template_name, {})