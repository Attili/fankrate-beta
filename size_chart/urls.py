from django.urls import path
from django.conf.urls import url

from .views import (SizeChart)

urlpatterns = [
        path('', SizeChart.as_view(), name='size_chart'),
]
