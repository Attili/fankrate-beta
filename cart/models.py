from django.db import models
from django.contrib.postgres.fields import JSONField
from users.models import User
from products.models import Products


class Cart(models.Model):
    product = models.ForeignKey(
        Products,
        on_delete=models.CASCADE)
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE)
    price = models.DecimalField(
        max_digits=15,
        decimal_places=2)
    quantity = models.IntegerField(
        help_text='Quantity of product',
        )
    total_price = models.DecimalField(
        max_digits=15,
        decimal_places=2)
    colour = models.CharField(
        max_length=255,
        blank=True,
        null=True)
    size = models.CharField(
        max_length=255,
        blank=True,
        null=True)
    phone_model = models.CharField(
        max_length=255,
        blank=True,
        help_text='Phone model name',
        null=True)

    class Meta:
        db_table = 'cart'
