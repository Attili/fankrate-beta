from django.shortcuts import render, redirect
from django.views.generic import View
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from products.models import ProductCategory, Products
import itertools
from .models import Cart
from django.template.loader import get_template
from django.template import Context, Template,RequestContext
import datetime
import hashlib
from random import randint
from django.views.decorators.csrf import csrf_protect, csrf_exempt
import requests
from decimal import Decimal
from users.mixins import LoginRequired, AdminPermission
from orders.models import Orders
from .tasks import send_cod_message
from django_q.tasks import async_task, result


class CartPage(LoginRequired,View):
    template_name = 'cart/index.html'
    def get(self, request):
        try:
            no_of_items = Cart.objects.filter(user=request.user).count()
        except Exception:
            no_of_items = 0
        cart_items = Cart.objects.filter(user=request.user)
        total_price = 0
        actual_total = 0
        total_quantity = 0
        all_products = []
        display_item_array = []
        for item in cart_items:
            dict = {
                'name': item.product.name,
                'size': item.size,
                'quantity': item.quantity,
                'colour': item.colour,
                'price': float(item.total_price),
                'id': item.id,
                'image': item.product.images[str(item.colour)]
            }
            display_item_array.append(dict)

        for cart_item in cart_items:
            actual_total += (cart_item.product.price * cart_item.quantity)
            total_price += cart_item.total_price
            total_quantity += cart_item.quantity
            all_products.append({"product":cart_item.product.id,
                                 "quantity": cart_item.quantity,
                                 "color": cart_item.colour,
                                 "size": cart_item.size,
                                 "price": str(cart_item.product.selling_price * cart_item.quantity),
                                 "phone_model": cart_item.phone_model})
        total_price = round(total_price + total_price * Decimal(0.03), 2)
        internet_charges = round(total_price * Decimal(0.03), 2)
        savings = round(actual_total - total_price, 2)
        shipping = 0.00

        return render(request, self.template_name, {'cart_items': cart_items,
                                                    'display_item_array':display_item_array,
                                                    'total_price': total_price,
                                                    'actual_total': actual_total,
                                                    'savings': savings,
                                                    'shipping': shipping,
                                                    'internet_charges': internet_charges,
                                                    'total_quantity': total_quantity,
                                                    'all_products': all_products,
                                                    'MERCHANT_KEY': 'keDgY9Uw',
                                                    'email': '',
                                                    'udf1': request.user.id,
                                                    'udf2': no_of_items,
                                                    'no_of_items': no_of_items,
                                                    'surl':'https://fankrate.com/orders/success/',
                                                    'furl':'https://fankrate.com/orders/fail/'})

    def post(self, request):
        MERCHANT_KEY = "keDgY9Uw"
        key = "keDgY9Uw"
        total_price = request.POST['amount']
        SALT = "4KTBBsy54G"
        email = request.POST['email']
        udf1 = request.POST['udf1']
        udf2 = request.POST['udf2']
        surl = request.POST['surl']
        furl = request.POST['furl']
        all_products = request.POST['productinfo']
        PAYU_BASE_URL = "https://secure.payu.in/_payment"
        posted = {}
        for i in request.POST:
            posted[i] = request.POST[i]
        hash_object = hashlib.sha256(b'randint(0,20)')
        txnid = hash_object.hexdigest()[0:20]
        posted['txnid'] = txnid
        hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10"
        posted['key'] = key
        hash_string = ''
        hashVarsSeq = hashSequence.split('|')
        for i in hashVarsSeq:
            try:
                hash_string += str(posted[i])
            except Exception:
                hash_string += ''
            hash_string += '|'
        hash_string += SALT
        hashh = hashlib.sha512(hash_string.encode('utf-8')).hexdigest().lower()

        return render(request, 'cart/index.html', {"posted": posted, "hashh": hashh,
                                                   "MERCHANT_KEY": MERCHANT_KEY,
                                                   "txnid": txnid,
                                                   "email": email,
                                                   "udf1": udf1,
                                                   "udf2": udf2,
                                                   "total_price": total_price,
                                                   "all_products": all_products,
                                                   "surl": surl,
                                                   "furl": furl,
                                                   "hash_string": hash_string,
                                               "action": PAYU_BASE_URL})



class COD(LoginRequired,View):

    def post(self, request):
        amount = request.POST['amount']
        firstname = request.POST['firstname']
        productinfo = request.POST['productinfo']
        date_1 = (datetime.date.today() + datetime.timedelta(days=4)).strftime("%d %b %Y")
        date_2 = (datetime.date.today() + datetime.timedelta(days=6)).strftime("%d %b %Y")
        order_id = 'FANK' + str(randint(1000, 99999))

        Orders.objects.create(user=request.user,
                              transaction_id="COD",
                              total_price=amount,
                              structure=productinfo,
                              order_id=order_id,
                              pincode=request.POST['zipcode'],
                              payment_status=4,
                              delivery_status=1,
                              order_type=2,
                              no_of_items=request.POST["udf2"],
                              tracking_id="",
                              mobile=request.POST['phone'],
                              expected_delivery=date_1 + ' - ' + date_2,
                              design_sku='',
                              profit=0,
                              address=str(firstname + ", " + request.POST['address1']) + ", " + request.POST[
                                  'address2'] + ", " +
                                      request.POST['city'] + ", " + request.POST['state']
                              )
        cart_items = Cart.objects.filter(user=request.user)
        cart_items.delete()
        async_task(send_cod_message, request.POST['phone'], order_id, amount)
        return HttpResponseRedirect('/orders')

def remove(request):
    item = request.GET['item']
    cart_item = Cart.objects.get(id=item)
    cart_item.delete()
    cart_items = Cart.objects.filter(user=request.user)
    total_price = 0
    total_quantity = 0
    all_products = []

    for cart_item in cart_items:
        total_price += cart_item.total_price
        total_quantity += cart_item.quantity
        all_products.append({'product': cart_item.product.id,
                             'quantity': cart_item.quantity,
                             'color': cart_item.colour,
                             'phone_model': cart_item.phone_model})

    return HttpResponseRedirect('/cart')