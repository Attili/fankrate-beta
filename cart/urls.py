from django.urls import path
from django.conf.urls import url

from .views import (CartPage, remove, COD)

urlpatterns = [
        path('', CartPage.as_view(), name='cart'),
        path('remove/', remove, name='remove'),
        path('cod/', COD.as_view(), name='cod'),
]
