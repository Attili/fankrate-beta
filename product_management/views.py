from django.shortcuts import render, redirect
from django.views.generic import View
from django.http import HttpResponseRedirect
from django.urls import reverse
from users.models import User
from products.models import Products, ProductCategory
import requests
import json
from django_q.tasks import async_task, result
from users.mixins import LoginRequired, AdminPermission


class ProductManagement(AdminPermission, View):
    template_name = 'product_management/index.html'

    def get(self, request):
        return render(request, self.template_name, {})


class AddProduct(AdminPermission, View):
    template_name = 'product_management/add_product.html'

    def get(self, request):
        product_categories = ProductCategory.objects.all()
        return render(request, self.template_name, {'product_categories': product_categories})

    def post(self, request):
        data = request.POST

        name = data.get('name', None)
        description = data.get('description', None)
        images = data.get('images', None)
        price = data.get('price', None)
        selling_price = data.get('selling_price', None)
        actual_price = data.get('actual_price', None)
        csq = data.get('csq', None)
        is_deleted = data.get('is_deleted', None)
        in_stock = data.get('in_stock', None)
        is_cod_available = data.get('is_cod_available', None)
        is_dropship_product = data.get('is_dropship_product', None)
        design_sku = data.get('design_sku', None)
        phone_model = data.get('phone_model', None)
        product_category = data.get('product_category', None)
        profit = float(selling_price) - float(actual_price)

        images = json.loads(images)
        csq = json.loads(csq)

        if is_deleted:
            is_deleted = True
        else:
            is_deleted = False

        if in_stock:
            in_stock = True
        else:
            in_stock = False

        if is_cod_available:
            is_cod_available = True
        else:
            is_cod_available = False

        if is_dropship_product:
            is_dropship_product = True
        else:
            is_dropship_product = False

        product_category = ProductCategory.objects.get(id=product_category)

        Products.objects.get_or_create(name=name,
                                       description=description,
                                       images=images,
                                       price=price,
                                       selling_price=selling_price,
                                       actual_price=actual_price,
                                       profit=profit,
                                       csq=csq,
                                       is_deleted=is_deleted,
                                       in_stock=in_stock,
                                       is_cod_available=is_cod_available,
                                       is_dropship_product=is_dropship_product,
                                       design_sku=design_sku,
                                       phone_model=phone_model,
                                       product_category=product_category)

        return render(request, self.template_name, {})


class EditProduct(AdminPermission, View):
    template_name = 'product_management/index.html'

    def get(self, request):
        return render(request, self.template_name, {})
