from django.urls import path
from django.conf.urls import url

from .views import (AddProduct,
                    ProductManagement,
                    EditProduct)

urlpatterns = [
        path('', ProductManagement.as_view(), name='product_management'),
        path('add_product/', AddProduct.as_view(), name='add_product'),
        path('edit_product/', EditProduct.as_view(), name='edit_product'),
]
