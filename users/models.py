from django.db import models
from django.contrib.auth.models import (
    BaseUserManager,
    AbstractBaseUser
)


class UserManager(BaseUserManager):
    def create_user(self, mobile, name, password=None):

        user = self.model(
            mobile=mobile,
            name=name,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, mobile, name, password):
        user = self.create_user(mobile, password=password, name=name)
        user.is_admin = True
        user.is_staff = True
        user.role = 'ADMIN'
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    mobile = models.CharField(
        verbose_name='mobile',
        max_length=20,
        unique=True)
    otp = models.CharField(
        max_length=6,)
    is_verified = models.BooleanField(
        default=False)
    name = models.CharField(
        max_length=255,
        blank=False)
    is_staff = models.BooleanField(default=False)
    is_admin = models.BooleanField(
        default=False)
    added_on = models.DateTimeField(
        auto_now_add=True)
    updated_on = models.DateTimeField(
        auto_now=True)
    role = models.CharField(
        max_length=20,
        help_text='Role of users',
        default='USER'
    )
    no_of_orders = models.IntegerField(
        blank=False,
        default=0)

    USERNAME_FIELD = 'mobile'
    REQUIRED_FIELDS = ['name']

    objects = UserManager()

    def has_module_perms(self, app_label):
        'Does the users have permissions to view the app `app_label`?'
        # Simplest possible answer: Yes, always
        return True

    def has_perm(self, *args, **kwargs):
        return True

    class Meta:
        db_table = 'users'



