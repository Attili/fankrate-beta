import requests
from .models import User
from random import randint


def send_otp(mobile):
    otp = randint(100000, 999999)

    user = User.objects.get(mobile=mobile)
    user.otp = otp
    user.save()

    URL = 'https://www.sms4india.com/api/v1/sendCampaign'
    req_params = {
        'apikey': 'Y6Q0QUFQGH51Y1P5NITMHNUDSLNGQ7TN',
        'secret': 'RSFK83D4XNLL6SP4',
        'usetype': 'prod',
        'phone': mobile,
        'message': 'Your FanKrate OTP:'+str(otp),
        'senderid': 'FANKRA'
    }
    requests.post(URL, req_params)