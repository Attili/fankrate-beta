from django.shortcuts import render, redirect
from django.views.generic import View
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse
from .models import User
from random import randint
from .tasks import send_otp
import requests
import json
from django_q.tasks import async_task, result


class Login(View):
    template_name = 'users/login.html'
    authentication_classes = ()

    def get(self, request):
        try:
            no_of_items = Cart.objects.filter(user=request.user).count()
        except Exception:
            no_of_items = 0
        if request.user.is_active:
            return HttpResponseRedirect(reverse('home_page'))
        return render(request, self.template_name, {'no_of_items': no_of_items})

    def post(self, request):
        data = request.POST

        mobile = data.get('mobile', None)
        password = data.get('password', None)

        user = authenticate(
            mobile=mobile,
            password=password)

        if user:
            if user.is_verified:
                login(request, user)
                request.session.save()
                context = {"first_name": user.name,
                           "mobile": user.mobile,
                           }
                return HttpResponseRedirect(reverse('home_page'))
            else:
                login(request, user)
                request.session.save()
                context = {'msg': 'Please confirm your mobile to continue'}
                return HttpResponseRedirect(reverse('otp_verify'))

        else:
            context = {'msg': 'Invalid mobile number or password'}
            return render(request, self.template_name, context)


class Signup(View):
    template_name = 'users/register.html'

    def get(self, request):
        if request.user.is_active:
            return HttpResponseRedirect(reverse('home_page'))
        return render(request, self.template_name, {})


class VerifyOTP(View):
    template_name = 'users/otp_verify.html'

    def get(self, request):
        if request.user.is_active and request.user.is_verified:
            return HttpResponseRedirect(reverse('home_page'))
        elif request.user.is_anonymous:
            return HttpResponseRedirect(reverse('home_page'))
        return render(request, self.template_name, {})

    def post(self, request):
        data = request.POST
        user = request.user
        otp = data['otp']

        if user.otp == otp:
            user.is_verified = True
            otp = randint(100000, 999999)
            user.otp = otp
            user.save()

            return HttpResponseRedirect(reverse('login'))
        else:
            context = {'msg': 'Invalid OTP'}
            return render(request, self.template_name, context)


class Saveuser(View):
    template_name = 'users/otp_verify.html'

    def get(self, request):
        return render(request, self.template_name, {})

    def post(self, request):
        data = request.POST
        mobile = data['mobile']
        name = data['name']
        password = data['password']
        password_again = data['password_again']

        if password == password_again:
            try:
                user = User.objects.get(mobile=mobile)
                login(request, user)
                request.session.save()
            except User.DoesNotExist:
                user = User()
                user.name = name
                user.mobile = mobile
                user.set_password(password)
                user.save()
                login(request, user)
                request.session.save()
                async_task(send_otp, mobile)

        else:
            context = {'msg': 'Passwords do not match'}
            return render(request, 'users/register.html', context)
        return render(request, self.template_name, {})


class ResendOTP(View):

    def get(self, request):
        user = request.user
        mobile = user.mobile
        async_task(send_otp, mobile)
        return HttpResponseRedirect(reverse('otp_verify'))


class Logout(View):

    def get(self, request):
        logout(request)
        return HttpResponseRedirect(reverse('home_page'))
