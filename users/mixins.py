from django.http import HttpResponseRedirect
from django.conf import settings

from django.urls import reverse
from users.models import User


class Permission(object):
    def not_logged_in(self, next=None):
        if next:
            return HttpResponseRedirect(settings.LOGIN_URL + '?next=' + next)
        else:
            return HttpResponseRedirect(settings.LOGIN_URL)


class LoginRequired(Permission):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and request.user.is_verified:
            return super(LoginRequired, self).dispatch(request, *args, **kwargs)
        elif request.user.is_authenticated and not request.user.is_verified:
            return HttpResponseRedirect(settings.LOGOUT_URL)
        else:
            return self.not_logged_in(
                next=request.get_full_path())


class AdminPermission(Permission):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and request.user.is_admin:
            return super(AdminPermission, self).dispatch(request, *args, **kwargs)
        elif request.user.is_anonymous:
            return self.not_logged_in(
                next=request.get_full_path())
        else:
            return self.not_logged_in(
                next=request.get_full_path())


