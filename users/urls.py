from django.urls import path
from django.conf.urls import url

from .views import (Login,
                    Signup,
                    Saveuser,
                    VerifyOTP,
                    ResendOTP,
                    Logout
                    )

urlpatterns = [
        path('login/', Login.as_view(), name='login'),
        path('logout/', Logout.as_view(), name='logout'),
        path('signup/', Signup.as_view(), name='signup'),
        path('saveuser/', Saveuser.as_view(), name='save_user'),
        path('otp_verify/', VerifyOTP.as_view(), name='otp_verify'),
        url('resend_otp/', ResendOTP.as_view(), name='resend_otp'),
]
