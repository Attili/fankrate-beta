from django.urls import path
from django.conf.urls import url

from .views import (AllOrders)

urlpatterns = [
        path('', AllOrders.as_view(), name='all_orders'),
]
