from django.shortcuts import render, redirect
from django.views.generic import View
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from products.models import ProductCategory, Products
import itertools
from orders.models import Orders
from django.template.loader import get_template
from django.template import Context, Template,RequestContext
import datetime
import hashlib
from random import randint
from django.views.decorators.csrf import csrf_protect, csrf_exempt
import requests
import json



class AllOrders(View):
    template_name = 'order_management/index.html'
    authentication_classes = ()

    def get(self, request):
        try:
            orders = Orders.objects.all().order_by('payment_status')
        except Exception:
            orders = None

        orders_array = []
        for order in orders:
            all_orders = {}
            all_orders['address'] = order.address
            all_orders['pincode'] = order.pincode
            all_orders['mobile'] = order.mobile
            all_orders['order_id'] = order.order_id
            all_orders['total_price'] = order.total_price
            all_orders['transaction_id'] = order.transaction_id
            all_orders['tracking_id'] = order.tracking_id
            all_orders['expected_delivery'] = order.expected_delivery
            all_orders['payment_status'] = Orders.payment_status_options[order.payment_status - 1][1]
            all_orders['delivery_status'] = Orders.delivery_status_options[order.delivery_status - 1][1]
            structure = order.structure.replace("\'", "\"")
            structure = json.loads(structure)
            product_details = []
            for dict in structure:
                product = Products.objects.get(id=dict['product'])
                product_category = ProductCategory.categories[product.product_category.category - 1][1]
                p = {
                    'quantity': dict['quantity'],
                    'phone_model': dict['phone_model'],
                    'color': dict['color'],
                    'product': product.name,
                    'price': dict['price'],
                    'size': dict['size'],
                    'product_image': product.images[str(dict['color'])],
                    'product_category': product_category
                }
                product_details.append(p)
            all_orders['product_details'] = product_details
            all_orders['id'] = order.id
            orders_array.append(all_orders)
        return render(request, self.template_name, {'orders_array': orders_array})
