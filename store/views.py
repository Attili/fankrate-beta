from django.shortcuts import render, redirect
from django.views.generic import View
from django.http import HttpResponseRedirect
from django.urls import reverse
from products.models import ProductCategory, Products
import itertools
from cart.models import Cart


class Home(View):
    template_name = 'store/index.html'
    authentication_classes = ()

    def get(self, request):
        try:
            no_of_items = Cart.objects.filter(user=request.user).count()
        except Exception:
            no_of_items = 0
        return render(request, self.template_name, {'no_of_items': no_of_items})


class ProductList(View):
    template_name = 'store/product_list.html'
    authentication_classes = ()

    def get(self, request):
        product_category = request.GET['product_category']
        try:
            no_of_items = Cart.objects.filter(user=request.user).count()
        except Exception:
            no_of_items = 0
        try:
            products = Products.objects.filter(product_category=product_category)
            category = ProductCategory.categories[int(product_category) - 1][1]
        except Products.DoesNotExist:
            products = {}
            category = ''

        products_array = []
        for product in products:
            image_link = ''
            for k,v in product.images.items():
                image_link = v
            dict = {
                'selling_price': product.selling_price,
                'price': product.price,
                'name': product.name,
                'image': image_link,
                'id': product.id,
            }
            products_array.append(dict)

        return render(request, self.template_name, {'products': products_array,
                                                    'category': category,
                                                    'no_of_items': no_of_items})


class ProductView(View):
    template_name = 'store/product_view.html'
    authentication_classes = ()

    def get(self, request):
        product = request.GET['product']
        try:
            no_of_items = Cart.objects.filter(user=request.user).count()
        except Exception:
            no_of_items = 0
        try:
            product = Products.objects.get(id=product)
            category = ProductCategory.categories[int(product.product_category.category)-1][1]
        except Products.DoesNotExist:
            product = None
            category = ''
        images = []
        main_image = ''
        if product:
            for color, image_link in product.images.items():
                images.append(image_link)
                main_image = image_link
        return render(request, self.template_name, {'product': product,
                                                    'category': category,
                                                    'main_image': main_image,
                                                    'no_of_items': no_of_items,
                                                    'images': images})

    def post(self, request):
        if request.user.is_active:
            product = request.GET['product']
            try:
                size = request.POST['size']
            except Exception:
                size = None
            color = request.POST['color']
            quantity = request.POST['quantity']
            product = Products.objects.get(id=product)
            phone_model = product.phone_model
            price = product.selling_price
            total_price = float(price) * int(quantity)
            Cart.objects.create(user=request.user,
                                total_price=total_price,
                                price=price,
                                colour=color,
                                product=product,
                                quantity=quantity,
                                size=size,
                                phone_model=phone_model
                                )

            return HttpResponseRedirect(reverse('cart'))
        return HttpResponseRedirect(reverse('login'))

