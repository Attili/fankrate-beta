from django.urls import path
from django.conf.urls import url

from .views import (Home, ProductList, ProductView)

urlpatterns = [
        path('', Home.as_view(), name='home_page'),
        path('products/', ProductList.as_view(), name='products'),
        path('view-product/', ProductView.as_view(), name='view_product'),
]
