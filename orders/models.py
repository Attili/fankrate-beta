from django.db import models
from django.contrib.postgres.fields import JSONField
from users.models import User


class Orders(models.Model):
    prepaid = 1
    cod = 2
    sample = 3
    free_order = 4

    order_types = (
        (prepaid, 'Prepaid'),
        (cod, 'COD'),
        (sample, 'Sample'),
        (free_order, 'Free Order'))

    processing = 1
    successful = 2
    failed = 3
    pending = 3

    payment_status_options = (
        (processing, 'Processing'),
        (successful, 'Successful'),
        (failed, 'Failed'),
        (pending, 'Pending'))

    order_processing = 1
    packed = 2
    dispatched = 3
    in_transit = 4
    out_for_delivery = 5
    delivered = 6

    delivery_status_options = (
        (order_processing, 'Processing'),
        (packed, 'Packed'),
        (dispatched, 'Dispatched'),
        (in_transit, 'In transit'),
        (out_for_delivery, 'Out for delivery'),
        (delivered, 'Delivered'))

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE)
    structure = JSONField(
        help_text='Product, quantity, amount, color, size and total amount Json')
    total_price = models.DecimalField(
        max_digits=15,
        decimal_places=2)
    profit = models.DecimalField(
        max_digits=15,
        decimal_places=2)
    no_of_items = models.IntegerField(
        help_text='Quantity of product',
        )
    tracking_id = models.CharField(
        max_length=255,
        blank=True,
        null=True)
    order_id = models.CharField(
        max_length=255,
        blank=True,
        null=True)
    transaction_id = models.CharField(
        max_length=255,
        blank=True,
        null=True)
    address = models.CharField(
        max_length=255,
        blank=True,
        null=True)
    pincode = models.CharField(
        max_length=255,
        blank=True,
        null=True)
    expected_delivery = models.CharField(
        max_length=255,
        blank=True,
        null=True)
    mobile = models.CharField(
        max_length=255,
        blank=True,
        help_text='Mobile',
        null=True)
    order_type = models.IntegerField(
        choices=order_types,
        null=True)
    payment_status = models.IntegerField(
        choices=payment_status_options,
        null=True)
    delivery_status = models.IntegerField(
        choices=delivery_status_options,
        null=True)
    design_sku = models.CharField(
        max_length=255,
        blank=True)

    class Meta:
        db_table = 'orders'
