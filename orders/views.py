from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from django.template.loader import get_template
from django.template import Context, Template,RequestContext
import datetime
import hashlib
import json
from random import randint
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.template.context_processors import csrf
from orders.models import Orders
from cart.models import Cart
from django.views.generic import View
from products.models import (Products, ProductCategory)
from users.mixins import LoginRequired, AdminPermission
import random
from .tasks import send_order_confirmation
from django_q.tasks import async_task, result
from users.models import User


class OrdersView(LoginRequired, View):
    template_name = 'orders/index.html'

    def get(self, request):
        try:
            no_of_items = Cart.objects.filter(user=request.user).count()
        except Exception:
            no_of_items = 0
        orders = Orders.objects.filter(user=request.user).order_by('-id','payment_status')
        orders_array = []
        for order in orders:
            all_orders = {}
            all_orders['address'] = order.address
            all_orders['pincode'] = order.pincode
            all_orders['mobile'] = order.mobile
            all_orders['order_id'] = order.order_id
            all_orders['total_price'] = order.total_price
            all_orders['transaction_id'] = order.transaction_id
            all_orders['tracking_id'] = order.tracking_id
            all_orders['expected_delivery'] = order.expected_delivery
            all_orders['payment_status'] = Orders.payment_status_options[order.payment_status-1][1]
            all_orders['delivery_status'] = Orders.delivery_status_options[order.delivery_status-1][1]
            structure = order.structure.replace("\'", "\"")
            structure = json.loads(structure)
            product_details = []
            for dict in structure:
                product = Products.objects.get(id=dict['product'])
                product_category = ProductCategory.categories[product.product_category.category-1][1]
                p ={
                    'quantity': dict['quantity'],
                    'phone_model': dict['phone_model'],
                    'color': dict['color'],
                    'product': product.name,
                    'price': dict['price'],
                    'size': dict['size'],
                    'product_image': product.images[str(dict['color'])],
                    'product_category': product_category
                }
                product_details.append(p)
            all_orders['product_details'] = product_details
            all_orders['id'] = order.id
            orders_array.append(all_orders)
        return render(request, self.template_name, {'orders_array': orders_array,
                                                    'orders': orders,
                                                    'no_of_items': no_of_items})



@csrf_protect
@csrf_exempt
def success(request):
    c = {}
    c.update(csrf(request))
    status=request.POST["status"]
    firstname=request.POST["firstname"]
    amount=request.POST["amount"]
    txnid=request.POST["txnid"]
    posted_hash=request.POST["hash"]
    key=request.POST["key"]
    productinfo=request.POST["productinfo"]
    email=request.POST["email"]
    udf1=request.POST["udf1"]
    udf2=request.POST["udf2"]
    salt="4KTBBsy54G"
    order_id_base = 'FANK'
    try:
        additionalCharges=request.POST["additionalCharges"]
        retHashSeq=additionalCharges+'|'+salt+'|'+status+'|||||||||||'+email+'|'+firstname+'|'+productinfo+'|'+amount+'|'+txnid+'|'+key
    except Exception:
        retHashSeq = salt+'|'+status+'|||||||||'+udf2+'|'+udf1+'|'+email+'|'+firstname+'|'+productinfo+'|'+amount+'|'+txnid+'|'+key
    hashh=hashlib.sha512(retHashSeq.encode('utf-8')).hexdigest().lower()

    if(hashh !=posted_hash):
        print("Invalid Transaction. Please try again")
    else:
        payment_status = 2 if status == "success" else 3
        date_1 = (datetime.date.today() + datetime.timedelta(days=4)).strftime("%d %b %Y")
        date_2 = (datetime.date.today() + datetime.timedelta(days=6)).strftime("%d %b %Y")
        order_id = order_id_base + str(randint(1000, 99999))
        user = User.objects.get(id=request.POST["udf1"])
        Orders.objects.create(user=user,
                              transaction_id=request.POST['payuMoneyId'],
                              total_price=amount,
                              structure=productinfo,
                              order_id=order_id,
                              pincode=request.POST['zipcode'],
                              payment_status=payment_status,
                              delivery_status = 1,
                              order_type=1,
                              no_of_items=request.POST["udf2"],
                              tracking_id="",
                              mobile=request.POST['phone'],
                              expected_delivery=date_1+' - '+date_2,
                              design_sku='',
                              profit=0,
                              address=str(firstname + ", " +request.POST['address1'])+", "+request.POST['address2']+", "+
                                      request.POST['city']+", "+request.POST['state']
                              )

        cart_items = Cart.objects.filter(user=user)
        cart_items.delete()
        async_task(send_order_confirmation, request.POST['phone'], order_id, amount)
    return HttpResponseRedirect('/orders')


@csrf_protect
@csrf_exempt
def failed(request):

    return HttpResponseRedirect('/cart')