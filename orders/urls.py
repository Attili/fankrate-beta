from django.conf.urls import include, url
from django.contrib import admin
from .views import (success, OrdersView, failed)
from django.urls import path

urlpatterns = [
    path('', OrdersView.as_view(), name='orders_view'),
    path('success/', success, name='success'),
    path('fail/', failed, name='fail'),

]